#!/bin/bash

# Source the profile.sh file to get profile info
source .profile.sh

# entry point
main () {
    log
}

# Listen function to get user command
waitForPrompt () {
    # Reading and storing command plus parameter
    read command param
    echo ~
    # Case to stores all our commands
    case $command in

        "help") help;;
        "ls") ls $param;;
        "rm") remove "$param";;
        "rmd" | "rmdir") removeDir;;
        "about") about;;
        "version" | "--v") version;;
        "age") age;;
        "quit") quit;;
        "profile") profile;;
        "passw") passw $param;;
        "cd") cd $param;;
        "pwd") pwd;;
        "ahour") ahour;;
        "httpget") httpget $param;;
        "smtp") smtp;;
        "open") open $param;; 
        *) echo "command undefined"  
    esac
        # Re-call wait for prompt
    waitForPrompt  
}

# Log function to check if user type good info
log () {
    echo Hello, who am i talking to ?

    read -p 'Username: ' uservar
    read -sp 'Password: ' passvar

    # If login not good clear console display an error and re-ask for login
    if [[ "$uservar" != "$login" ]]; then
        clear
        echo Acces denied user didnt exist
        log
    fi 
    # If password not good clear console display an error and re-ask for password
    if [[ $passvar != $pass ]]; then
        clear
        echo Acces denied wrong password
        log
    fi 
    # If all good display a succed message and ask for prompt
        clear
        echo Thank you $uservar welcome to the Magic Prompt !, just type command to execute them
        waitForPrompt
    
}

# Display the list of commands
help () {
    echo "
        help : qui indiquera les commandes que vous pouvez utiliser
        ls : lister des fichiers et les dossiers visible comme caché
        rm : supprimer un fichier
        rmd ou rmdir : supprimer un dossier
        about : une description de votre programme
        version ou --v ou vers :  affiche la version de votre prompt
        age : vous demande votre âge et vous dit si vous êtes majeur ou mineur
        quit : permet de sortir du prompt
        profile : permet d’afficher toutes les informations sur vous même.
        First Name, Last name, age, email
        passw : permet de changer le password avec une demande de confirmation
        cd : aller dans un dossier que vous venez de créer ou de revenir à un dossier 
        précédent
        pwd : indique le répertoire actuelle courant
        hour : permet de donner l’heure actuelle
        * : indiquer une commande inconnu
        httpget : permet de télécharger le code source html d’une page web et de l’enregistrer dans un fichier spécifique. Votre prompt doit vous demander quel sera le nom du fichier.
        smtp : vous permet d’envoyer un mail avec une adresse un sujet et le corp du mail
        open : ouvrir un fichier directement dans l’éditeur VIM même si le fichier n’existe pas"
}

# smtp function will send a email to a user param needed : target email
smtp () {
    echo Chose a target email
    read targetEmail
    echo Tell us the subject
    read subject
    echo and your body
    read body
    printf "Subject: ${subject}\n/\${body}" | ssmtp $targetEmail
 
}

# Open vim with ou whitout file
open () {
    vim ${param}
}

# Get the source code of a web page and store it in file.txt
httpget () {
     curl  $param > file.txt
}

# display the current time
ahour () {
    now=$(date +"%T")
    echo "Current time : $now"
}

# display the current absolute path
pwd () {
    builtin pwd
}

# Navigate throught directory
cd () {
    builtin cd "$param";   
}

# display user info store in the .profile.sh file
profile () {
    echo "First name : ${firstName}"
    echo "Last name : ${lastName}"
    echo "age : ${age}"
    echo "email : ${email}"  
}

# Exit the prompt
quit () {
    exit
}

# Change the value of pass variable in .profile.sh
passw () {
    sed_param=s/pass=.*/pass="${param}"/  
    sed -i "$sed_param" .profile.sh
}

# Ask for the age of the user and tell him if he is adult or child
age () {
    echo what is your age ?
    read age
    if [[ $age < "18" ]]; then
        echo You are an adult
    else
        echo You are a child
    fi
}

# Display the version of the prompt
version () {
    echo "my-magic-prompt:8.2"
}

# Display what in the folder
ls () {
    command ls -al $param
}

# Delete a file
remove () {
    rm $param
}

# Delete a folder
removeDir () {
    rm -d $param
}

# Display my magic prompt about
about () {
    echo Welcome to the magic prompt you can use a list of command for the list type help
}

# Call the main function
main



